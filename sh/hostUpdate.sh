#!/bin/bash

#put dns file
./dossh.sh put router ../conf /etc/dnsmasq.d hosts.conf

#Restart dns
./dossh.sh run router killall -9 dnsmasq
./dossh.sh run router dnsmasq &
