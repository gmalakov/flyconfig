#!/bin/bash

WIFIZONE="192.168.244.0/24"
#Enabled addresses
ENABLED=( "192.168.244.21" "192.168.244.32" "192.168.244.154" )

#Flush rules
iptables -F INPUT
iptables -F FORWARD

#Remove access to local resources
iptables -A INPUT -s $WIFIZONE -j DROP
#Grant access to router/dns
iptables -A FORWARD -s $WIFIZONE -d 192.168.254.1 -p udp --dport 53 -j ACCEPT
#Grant access to outside networks
iptables -A FORWARD -s $WIFIZONE ! -d 192.168.0.0/16 -j ACCEPT
#Remove access to everywhere else
iptables -A FORWARD -s $WIFIZONE -j DROP

#Enabled hosts
  len=${#ENABLED[@]}
  for (( i=0; i<${len}; i++))
    do
     iptables -I INPUT -s  ${ENABLED[$i]} -j ACCEPT
     iptables -I FORWARD -s ${ENABLED[$i]} -j ACCEPT
    done



#Add custom host for access
#iptables -I INPUT -s $ipenable -j ACCEPT
#iptables -I FORWARD -s $ipenable -j ACCEPT

#iptables -L -v
#count rows  -> 147 12464 ACCEPT     tcp  --  any    any     anywhere             anywhere             tcp dpt:22005

#Chain INPUT
#pkts bytes target     prot opt in     out     source               destination
#<packets> <bytes> <target> <prot> <options> <in> <out> <source> <destination> <proto> <flags and other>

