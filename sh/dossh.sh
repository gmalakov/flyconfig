#!/bin/bash

function arpGet {
   local ip="$1"
   ssh -tt -p $port $user@$address "arp -e |grep $ip" 1>tmp1.tmp
   res=$(cat tmp1.tmp)
   rm -rf tmp1.tmp
 }

function run {
   local cmd="$1"
   ssh -tt -p $port $user@$address "$cmd" 1>tmp1.tmp
   res=$(cat tmp1.tmp)
   rm -rf tmp1.tmp
 }

 function put {
   local lfolder="$1"
   local tfolder="$2"
   local file="$3"
   scp -P $port $lfolder/$file $user@$address:$tfolder/$file
   res="OK"
 }
 function putpath {
   local lfolder="$1"
   local tfolder="$2"
   scp -P $port $lfolder $user@$address:$tfolder
   res="OK"
 }

 function get {
   local tfolder="$1"
   local lfolder="$2"
   local file="$3"
   scp -P $port $user@$address:$tfolder/$file $lfolder/$file
   res="OK"
 }

 function getpath {
   local tfolder="$1"
   local lfolder="$2"
   scp -P $port $user@$address:$tfolder $lfolder
   res="OK"
 }

#Is it on router
if [ "$2" = "router" ];
 then
   . ./vars_router.sh
 else
   . ./vars_fs.sh
 fi


case "$1" in
 'arpget')
   arpGet $3
   echo $res
  ;;

  'put')
    put $3 $4 $5
    echo $res
  ;;

  'putpath')
    putpath $3 $4
    echo $res
  ;;

  'get')
    get $3 $4 $5
    echo $res
  ;;

  'getpath')
    get $3 $4
    echo $res
  ;;

  'run')
    cmd="$3"
    shift;shift;shift;
    while [ "$1" != "" ]; do
      cmd="$cmd $1" && shift;
    done;

    run "$cmd"
    echo $res
  ;;

 esac