#!/bin/bash

#Router
./dossh.sh put router ../conf /etc/dnsmasq.d hosts.conf
./dossh.sh put router ../conf /etc/dhcp dhcpd.conf
./dossh.sh put router ../sh  /home/scripts rmulti_ips.sh

./dossh.sh run router killall -9 dnsmasq
./dossh.sh run router dnsmasq &
./dossh.sh run router /etc/init.d/isc-dhcp-server restart
./dossh.sh run router /home/scripts/rmulti.sh start

#FS
#put route rules to flyfs (wifi)
./dossh.sh putpath flyfs route_fs.sh /scripts/route_fs.sh
#run route rules to flyfs (wifi)
./dossh.sh run flyfs /scripts/route_fs.sh
./dossh.sh putpath flyfs ../conf/dhcpd_fs.conf /etc/dhcp/dhcpd.conf

./dossh.sh run flyfs /etc/init.d/isc-dhcp-server restart
