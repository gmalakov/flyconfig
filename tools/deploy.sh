#!/bin/bash

#Run config get first
#./get.sh

web=../templates
bin=../bin
tools=../tools
dpath=/home/soft/dart-sdk/bin

tuser="user"
taddress="192.168.254.1"
tfolder="/home/user/flyconf"

target="$tuser@$taddress:$tfolder"
port=22005

#Create snapshot
echo "Creating snapshot ..."
cd $bin
./build.sh
cd $tools

ssh -t -p $port $tuser@$taddress "mkdir $tfolder"
ssh -t -p $port $tuser@$taddress "mkdir $tfolder/bin"
ssh -t -p $port $tuser@$taddress "mkdir $tfolder/conf"
ssh -t -p $port $tuser@$taddress "mkdir $tfolder/log"
ssh -t -p $port $tuser@$taddress "mkdir $tfolder/sh"

ssh -t -p $port $tuser@$taddress "mkdir $tfolder/templates"
ssh -t -p $port $tuser@$taddress "mkdir $tfolder/templates/css"
ssh -t -p $port $tuser@$taddress "mkdir $tfolder/templates/js"


echo "Copying data to server ..."
#Copy all web folders
scp -P $port $web/js/* $target/templates/js
scp -P $port $web/css/* $target/templates/css
scp -P $port $web/* $target/templates

#Config files and sh files
scp -P $port ../conf/* $target/conf
scp -P $port ../sh/* $target/sh
#Make sh files executable
ssh -t -p $port $tuser@$taddress "chmod +x $tfolder/sh/*"

#Copy snapshot to server
scp -P $port $bin/main.snapshot $target/bin
#Copy startups
scp -P $port $bin/st*sh $target/bin
#Make startup script executable
ssh -t -p $port $tuser@$taddress "chmod +x $tfolder/bin/st*sh"

echo "Checking DART version ..."
#check local dart version
dart --version 2>./tmp.tmp
lversion=$(cat tmp.tmp)
rm -rf tmp.tmp
ssh -t -p $port $tuser@$taddress "$dpath/dart --version" 1>tmp2.tmp
rversion=$(cat tmp2.tmp)
rm -rf tmp2.tmp

lvarr=( $lversion )
rvarr=( $rversion )

#echo "versions"
#echo ${lvarr[3]}
#echo ${rvarr[3]}

if [ "${lvarr[3]}" != "${rvarr[3]}" ];
 then
   echo "Dart version mismatch. Local version is ${lvarr[3]}, remote is ${rvarr[3]} ."
   echo 'Updating dart ...'
   #copy dart update script to server
   scp -P $port dupdate.sh /home/soft

   ssh -t -p $port root@$taddress << HERE
     cd /home/soft
     ./dupdate.sh
HERE
 fi

echo "Restarting process on server ..."
ssh -t -p $port $tuser@$taddress << HERE
  cd $tfolder/bin
  ./stNew.sh
HERE

cd ..
./push.sh
