#!/bin/bash

channel="$1"
version="$2"
prefix="$3"

#Default channel to stable
if [ "$channel" == "" ]; then
  channel="stable"
 fi

#Default version to latest
if [ "$version" == "" ]; then
  version="latest"
 fi

url=https://storage.googleapis.com/dart-archive/channels/$channel/release/$version/sdk/
#url=https://storage.googleapis.com/dart-archive/channels/$channel/release/latest/sdk/
bname=dartsdk-linux-x64-release.zip
durl=https://storage.googleapis.com/dart-archive/channels/$channel/release/$version/dartium/
#durl=https://storage.googleapis.com/dart-archive/channels/$channel/release/latest/dartium/
dname=dartium-linux-x64-release.zip

function do_comp {
    pref="$4"

    if [ "$pref" != "" ]; then
       pref="$pref/"
     fi

    #Remove archive if any
    rm -rf $pref$2
    #Remove folder
    rm -rf $pref$3
    #Go there
    if [ "$pref" != "" ]; then
       #Create prefix dir if not exist
       mkdir $4
       cd $4
     fi

    wget --no-check-certificate $1$2
    #Unzip archive
    unzip $2
    #remove archive
    rm -rf $2

    #Normalize name
    local dname=`ls -d $3*`
    if [ "$dname" != "$3" ]; then
      mv $dname $3
     fi
    #change mode
    chmod 777 $3 -R

    #return
    if [ "$pref" != "" ]; then
        cd ..
     fi
 }


echo "Usage: down.sh channel version prefix_path"
do_comp $url $bname dart-sdk $prefix
#do_comp $durl $dname dartium $prefix

