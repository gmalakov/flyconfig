import 'dart:async';

//import '../lib/probe.dart';
//import '../lib/hostReader.dart';
import '../lib/watch.dart';

//import 'package:tools/logger.dart';

Future<int> main() async {
  Watch watch = new Watch(null,null);
  await watch.loadFile();
  watch
    ..cRoute = 2
    ..rWatch = false;

  watch.setProbes([new WFail(DateTime.parse("2018-05-04 09:10:23"), 1),
  new WFail(DateTime.parse("2018-05-05 09:10:23"), 0),
  new WFail(DateTime.parse("2018-05-05 12:10:23"), 1),
  new WFail(DateTime.parse("2018-05-05 12:40:23"), 0),
  new WFail(DateTime.parse("2018-05-06 03:10:23"), 1),
  new WFail(DateTime.parse("2018-05-06 05:10:23"), 1),
  new WFail(DateTime.parse("2018-05-06 15:10:23"), 0),
  new WFail(DateTime.parse("2018-05-07 09:10:23"), 1),
  new WFail(DateTime.parse("2018-05-08 13:33:23"), 1),
  new WFail(DateTime.parse("2018-05-08 15:10:23"), 0),
  new WFail(DateTime.parse("2018-05-09 21:10:23"), 1),
  new WFail(DateTime.parse("2018-05-09 23:10:23"), 1),
  new WFail(DateTime.parse("2018-05-10 09:10:23"), 0),
  new WFail(DateTime.parse("2018-05-11 02:10:23"), 1),
  new WFail(DateTime.parse("2018-05-11 05:10:23"), 1),
  new WFail(DateTime.parse("2018-05-12 12:10:23"), 0),
  new WFail(DateTime.parse("2018-05-13 13:10:23"), 1),
  new WFail(DateTime.parse("2018-05-14 05:10:23"), 1),
  new WFail(DateTime.parse("2018-05-16 12:44:23"), 1),
  new WFail(DateTime.parse("2018-05-16 13:54:23"), 1),
  new WFail(DateTime.parse("2018-05-17 15:10:13"), 1),
  new WFail(DateTime.parse("2018-05-17 16:05:23"), 0),
  new WFail(DateTime.parse("2018-05-17 19:10:23"), 1),
  new WFail(DateTime.parse("2018-05-18 01:10:23"), 1),
  new WFail(DateTime.parse("2018-05-18 04:10:23"), 1),
  new WFail(DateTime.parse("2018-05-18 06:10:23"), 0),
  new WFail(DateTime.parse("2018-05-18 08:10:23"), 1),
  new WFail(DateTime.parse("2018-05-18 09:10:23"), 1),
  new WFail(DateTime.parse("2018-05-18 11:10:23"), 2),
  ]);// Testing purpose only;


  print (watch.getAllFails(DateTime.parse("2018-05-10 01:00:00"), new DateTime.now()));
/*
  runZoned(() async {
    Probe probe = new Probe()
      ..setTarget(1, "212.39.90.42")
      ..setTarget(3, "192.168.254.3")
      ..setTarget(2, "192.168.246.100");

    print(await probe.here(1));
    print(await probe.here(2));
    print(await probe.here(3));
  }, onError: (dynamic err) => LogError(err.toString()));
 */
  return 0;
}