#!/bin/bash

if [ "$1" == "" ]
 then
  read -p "input file:" hfile
 else
   hfile=$1
 fi  
 
#cat $hfile | while read elem
#do
#    echo "127.0.0.1     $elem" >> hosts
#    echo "add name $elem address=127.0.0.1" > mikrotik_host.txt
#done

#mikrotik enter menu
mikroprefix="ip dns static"
mikroadd="add name"

echo "$mikroprefix remove [find comment=\"ransomware\"]" > mikrotik_host.txt

for elem in $( cat $hfile ); do
    #echo "127.0.0.1     $elem" >> hosts
    echo "$mikroprefix $mikroadd $elem address=127.0.0.1" comment="ransomware" >> mikrotik_host.txt
done

echo "/quit" >> mikrotik_host.txt
