let dFormat = "dd.mm.yyyy HH:MM:ss";

function initInterval() {
    document.getElementById("selInterval").selectedIndex = 1;
    workInterval();
}

function getInterval() {
    handlers["/ipconf/getWatchInterval"] = function(msg) {
        let obj = JSON.parse(msg);
        createDraw(obj["perc"]);

        let ilog = document.getElementById("ilog");
        let logMsg = "";
        for (let i = 0; i < obj["log"].length; i++) {
            logMsg += "["+obj["log"][i][0]+"] ";
            switch (obj["log"][i][1]) {
                case 0:
                    logMsg += "Виваком е изгубен!";
                   break;
                case 1:
                    logMsg += "MTEL е изгубен!";
                   break;
                case 2:
                    logMsg += "Двата интернета са изгубени!";
                   break ;

            }

           logMsg += "\n";
        }
        ilog.innerHTML = logMsg;
    };

    let startD = document.getElementById("start_date").value;
    let endD = document.getElementById("end_date").value;
    let msg = '{"startD":"'+startD+'","endD":"'+endD+'"}';
    call_server(msg, '/ipconf/getWatchInterval');
}


function workInterval() {
    let select = document.getElementById("selInterval");
    let option = select.selectedIndex;

    let date = new Date();
    let endHolder = document.getElementById("end_date");
    let startHolder = document.getElementById("start_date");
    endHolder.value = date.format(dFormat);
    let resDate = date.format(dFormat);

    switch (option) {
        case 1:
            resDate = date.addInterval(-12, "hour").format(dFormat);
            break;
        case 2:
            resDate = date.addInterval(-1, "day").format(dFormat);
            break;
        case 3:
            resDate = date.addInterval(-2, "day").format(dFormat);
            break;
        case 4:
            resDate = date.addInterval(-4, "day").format(dFormat);
            break;
        case 5:
            resDate = date.addInterval(-1, "week").format(dFormat);
            break;
        case 6:
            resDate = date.addInterval(-2, "week").format(dFormat);
            break;

    }

    startHolder.value = resDate;
    getInterval();
}

