let busyflag = false;
let time_req;
let cs_obj = mkRequestObj();
let last_action = "";
let handlers = [];

function mkRequestObj() {
    let req_obj;
    let browser = navigator.appName;
    if(browser == "Microsoft Internet Explorer")
        req_obj = new ActiveXObject("Microsoft.XMLHTTP");
    else
        req_obj = new XMLHttpRequest();
    return req_obj
}

function chk_state() {
    let ntime_req = new Date().getTime();
    if (ntime_req - time_req > 10000) {
        cs_obj.onreadystatechange = function() {};
        busyflag = false;
        alert("Изтекло време!");
        return;
    }

    if (cs_obj.readyState == 4) {
        cs_obj.onreadystatechange = function() {};
        busyflag = false;
        let data = cs_obj.responseText;
        if (data != "OK" && handlers[last_action] == null) alert(data); //Bring response text if not OK!
         else
             if (handlers[last_action] != null) handlers[last_action](data); //if handler is set then call it
    }
}

function validIp(ip) {
    //Valid ip address ?
    let regex = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
    return regex.test(ip);
}

function validMac(mac) {
   let regex = /^([0-9A-F]{2}[:-]){5}([0-9A-F]{2})$/;
   return regex.test(mac);
}

function validNum(num) {
    let regex = /^[0-9]*\d$/;
    return regex.test(num);
}

function call_server(msg,act) {
    if (busyflag == true) {
        setTimeout("call_server('"+msg+"','"+act+"');", 1000);
        return;
    }

    busyflag = true;
    time_req = new Date().getTime();

    cs_obj.open('post', act);
    cs_obj.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    cs_obj.send(msg);
    cs_obj.onreadystatechange = function() { chk_state(); };
    last_action = act;
}
