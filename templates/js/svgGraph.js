let colors = ["#0f0", "#00f", "#f00"];
let draw;

function bar(perc, typ) {
    let rect = draw.rect(6, 51).opacity(0);
    rect.stroke({ width: 1 });
    rect.fill(colors[typ]);
    rect.move(perc*8, 50);

    rect.animate("1.5s").opacity(1);
}

function drawStats(stats) {
    //Work on stats
    let allctr = 0;
    for (let i in stats)
        allctr += stats[i]; //calculate all drops

    for (let i in stats)
        stats[i] = (stats[i]/allctr)*100; //Make all this in percents

    let j = 0;
    for (let i in stats)  {
        let pos = Math.round(6*stats[i]);
        let rect = draw.rect(pos, 15).opacity(0);
        rect.stroke({ width: 1});
        rect.fill(colors[i]);
        rect.move(2, 125+j*20); //15dots (10) + (5)
        rect.animate("1.5s").opacity(1);

        let txt = draw.text(Math.round(stats[i])+"%").move(pos + 10,125+j*20+5); //20 points interval
        txt.font({ family: "Verdana", size: 12})
            .fill('#000');

        j++;
    }
}

function createDraw(datadraw) {
    if (draw != null) {
        draw.clear();
        draw.remove();
    }

    draw = SVG('drawing').size(830,190); // 120 + Summary (3*20) + 10 reserve
    draw.width = "100%";
    draw.height = "100%";

    draw.line(2,1,2,101).stroke({ width: 1 });
    draw.line(2,101,810,101).stroke({ width: 1 });
    draw.polygon([2,0, 0,10, 4,10]).stroke({ width: 0 });
    draw.polygon([815,101,805,99,805,103]).stroke({ width: 0 });

    for (let i = 1; i < 8; i ++) {
        draw.line(100*i,101,100*i,107).stroke({ width: 1 });
        let txt = draw.text(i*12.5+"%").move(i*100+2,108);
        txt.font({ family: "Verdana", size: 12})
            .fill('#000');
    }

    //Will disappear in the end
    let stats = [];
    for (let i = 0; i < datadraw.length; i++) {
        let pos = Math.round(datadraw[i][0]);
        let typ = datadraw[i][1];

        if (stats[typ] == null) stats[typ] = 1;
        else stats[typ]++; //Add connection drop to statistics
        bar(pos, typ);
    }

    drawStats(stats);
}
