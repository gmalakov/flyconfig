#!/bin/bash

user="user"
bPath="/home/user/flyconf"
tpath="/tmp"
pidFile="/tmp/flyconf.pid"
logFile="$bPath/log/stdout.log"
dPath="/home/soft/dart-sdk/bin/dart"
sPath="$bPath/bin"
sName="main.snapshot"

base_stop() {
 echo "stopping script..."
 #read old pid from pid file
 opid=`cat $pidFile`
 #test if process exists in process list
 output=`ps aux | grep $opid`
 #echo $output
 output=`echo $output | grep -w dart`
 #echo $output
 if [ "$output" != "" ];
  then
   #if process exists remove old process
   kill -9 $opid
  else
   echo "Server is not started!"
  fi
}

base_start() {
 base_stop
 echo "starting script...";
 cd $sPath
 #start new server process
 cmd="$dPath $sName &> \"$logFile\" & echo \$!"

 ##Execute script
 if [ "$USER" != "$user" ];
   then
    su -c "$cmd" $user > "$pidFile"
   else
    $dPath $sName > $logFile & echo $! > $pidFile
   fi
}

case "$1" in
 'start')
  base_start
  ;;
 'stop')
  base_stop
  ;;
 'help')
  echo "st.sh start|stop|help. Default is start"
  ;;
 *)
  base_start
esac
