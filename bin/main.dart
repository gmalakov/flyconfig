import 'dart:async';
import '../lib/server.dart';

import 'package:tools/conf_reader.dart';
import 'package:tools/logger.dart';

Future main(List<String> args) async {
  /*var log = new Logins();
  await log.loadUsers();
  log.addUser('admin', 'Admin adminov', 'xxxxx');
  await log.saveUsers();*/

  //Read file paths
  ConfReader cR = new ConfReader('../conf/prg.conf');
  await cR.readFile();

  runZoned(() {
    new ConfServer(cR);
  }, onError: (dynamic e,dynamic s) {
    LogError("EXCEPTION: $e, $s");
  });

}
