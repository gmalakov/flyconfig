library render;

import 'dart:io';
import 'dart:async';
import 'dart:convert';

import 'package:IMAP/mime_types.dart';
import 'package:tools/logger.dart';
import 'package:forms/forms.dart';
import 'package:tools/conf_reader.dart';
import 'package:tools/tools.dart';

import 'shTools.dart';
import 'hostReader.dart';
import 'watch.dart';
import 'urls.dart';
import 'ipt_analyzer.dart';

class HostDef {
  String net;
  String subNet;
  String name;
  String ip;
}

class NetPar {
  DHCPConf dhcp;
  String srvName, netName;
  NetPar(this.dhcp, this.srvName, this.netName);
}

class RenderPages {
  static const String templatePath = "../templates";
  static const String mikroFile = "../conf/mikrotiks.yaml";
  DHCPConf _dhcp_rtr;
  DHCPConf _dhcp_fs;
  SHReader  _sh;
  HostReader _hReader;
  Watch _watch;
  ConfReader _conf;

  List<Mikrotik> mikrotiks;

  int getIdx(String key, String ip) {
    int idx = -1;
    List<String> area = null;
    if (_sh.vars[key] is String) area = new List()..add(_sh.vars[key] as String);

    if (_sh.vars[key] is List<dynamic>) _sh.vars[key] = (_sh.vars[key] as List<dynamic>).cast<String>();
    if (_sh.vars[key] is List<String>) area = _sh.vars[key] as List<String>;
      else area = new List();
    idx = area.indexOf(ip);

    //Save back
    _sh.vars[key] = area;
    return idx;
  }

  NetPar getSrv(String fNetName) {
    String srvName, netName;
    DHCPConf dhcp;
    
    if (fNetName.isNotEmpty) {
      String str = fNetName;
      int idx = str.indexOf("_");
      if (idx > 0) {
        //Decode server name
        srvName = str.substring(0, idx);
        netName = str.substring(idx+1, str.length);

        //Select dhcp server
        if (srvName == "router") dhcp = _dhcp_rtr;
        else dhcp = _dhcp_fs;
      } else netName = str;
    }

    return new NetPar(dhcp, srvName, netName);
  }



  void setFuncs() {
    FileMaps.instance.fPath = templatePath;

    DataMaps.instance.setFunc("srv404", (Map<String, dynamic> opt) {
      return new Future.value(opt);
    });

    DataMaps.instance.setFunc("login", (Map<String, dynamic> opt) {
      return new Future.value(opt);
    });

    DataMaps.instance.setFunc("forbidden", (Map opt) async {
      dynamic applyLocal = _conf.params["apply_blocked_locally"].toString();
      if (applyLocal == 'true') applyLocal = true;
       else applyLocal = false;

      return {"forbidden": _hReader.getForbidden(),
              "applyLocal": applyLocal
      };
    });

    DataMaps.instance.setFunc("watch", (Map opt) async {
      bool watch = _watch.rWatch;
      //print(_watch.toString());
      return
        {"do_watch": watch,
         "isr1": (_watch.cRoute == 0),
         "isr2": (_watch.cRoute == 1),
         "isrm": (_watch.cRoute == 2) };
    });

    DataMaps.instance.setFunc("inethosts", (Map opt) async {
      return {"inetaddr": _hReader.getInternet() };
    });

    DataMaps.instance.setFunc("tickets", (Map opt) async {
      List<String> ips = (await IptAnalyzer.getWifis("192.168.244")) ?? [];
      Map<String, dynamic> oMap = {};

      List<Map<String, String>> ticks = Tickets.instance.getTickets();

      for (int i = 0; i < ticks.length; i++)
        //Remove already given tickets from ip list
        if (ips.contains(ticks[i]["ip"])) ips.remove(ticks[i]["ip"]);

      List<Map<String, String>> ipL = [];
      for (int i = 0; i < ips.length; i++)
        ipL.add({"ip": ips[i]});

      oMap["wifiIps"] = ipL;
      oMap["tickets"] = ticks;

      //print(oMap.toString());
      return oMap;
    });

    DataMaps.instance.setFunc("globals", (Map opt) async {
      //Make data suitable for moustache display
      List<Map<String, dynamic>> gates = new List();
      List<String> area = (_sh.vars["RTARGETS"] as List<dynamic>).cast<String>();
      List<String> target = (_sh.vars["RGATES"] as List<dynamic>).cast<String>();
      for (int i = 0; i < area.length; i++) {
        List<String> tgt = area[i].trim().split("/");
        if (tgt.length == 1) tgt.add("24"); //Add default mask if not exists
        gates.add(
            <String, dynamic>{"id": i, "target": tgt[0], "mask": tgt[1], "gate": target[i]});
      }

      List<Map<String, dynamic>> aports = new List();
      area = (_sh.vars["APORTS"] as List<dynamic>).cast<String>();
      target = (_sh.vars["APROTO"] as List<dynamic>).cast<String>();
      for (int i = 0; i < area.length; i++)
        aports.add(<String, dynamic>{"id":i,
          "port": area[i],
          "proto": target[i],
          "ctr": i,
          "istcp": (target[i] == "tcp"),
          "isudp": (target[i] == "udp")
        });

      List<Map<String, dynamic>> mikros = new List();
      for (int i = 0; i < mikrotiks.length; i++)
        mikros.add(<String, dynamic> {
          'id': i,
          'ip': mikrotiks[i].ip,
          'port': mikrotiks[i].port,
          'name': mikrotiks[i].name ?? '',
          'blocked': mikrotiks[i].blockedSites ?? false
        });

      return <String, dynamic>
             {"IF1": _sh.vars["IF1"],
              "IF2": _sh.vars["IF2"],
              "GW1": _sh.vars["GW1"], //Gateway 1
              "GW2": _sh.vars["GW2"], //Gateway 2
              "SITE": _sh.vars["SITE"], //Site outside ip address
              "SRVIP": _sh.vars["SRVIP"], //Server ip address
              "gates": gates,
              "aports": aports,
              "mikrotiks": mikros,
              "glen": gates.length,
              "plen": aports.length,
              "mlen": mikrotiks.length,
              };
    });

    DataMaps.instance.setFunc("tree", (Map opt) {
      Map<String, dynamic> retMap = new Map<String, dynamic>();
      List<Map> networks = new List(); //create networks list
      retMap["networks"] = networks; //add it to return map

      void decodeFile(DHCPConf _dhcp, String srvName) {

        _dhcp.networks.forEach((netName, NetWork val) {
          Map<String, dynamic> cMap = new Map<String, dynamic>();
          networks.add(cMap);
          cMap["srvName"] = srvName;
          cMap["netName"] = srvName+"_"+netName;

          List<Map> subnets = new List(); //Create subnet list
          cMap["subnets"] = subnets; //add it to return map

          _dhcp.networks[netName].subNets.forEach((subNet, SubNet val) {
            Map<String, dynamic> cMap = new Map<String, dynamic>();
            subnets.add(cMap);
            cMap["subName"] = subNet;

            List<Map> hosts = new List(); //Create hosts list
            cMap["hosts"] = hosts; //Add it to return map

            _dhcp.networks[netName].subNets[subNet].hosts.forEach((hostName, Host val) {
              Map<String, dynamic> cMap = new Map<String, dynamic>();
              hosts.add(cMap);

              cMap["hostName"] = hostName;
              cMap["ip"] = val.ip;
              cMap["mac"] = val.mac;
            });
            hosts.sort((a,b) => (Host.ipnum(a["ip"] as String).compareTo(Host.ipnum(b["ip"] as String))));
          });
          //subnets.sort((a,b) => (a["subName"].hashCode > b["subName"].hashCode));
        });
      }

      decodeFile(_dhcp_rtr, "router");
      decodeFile(_dhcp_fs, "flyfs");

      //networks.sort((a,b) => (a["netName"].hashCode > b["netName"].hashCode));
      return new Future.value(retMap);
    });

    //Display port redirection
    DataMaps.instance.setFunc("reDir", (Map<String, dynamic> opt) async {
      //_dhcp.networks[netName].subNets[subName].hosts[hostName];
      //Create full hosts list for all inner subnets
      List<HostDef> hosts = new List();

      void sortHosts() {
        hosts.sort((a, b) =>
        ((a.ip).compareTo(b.ip)));
      }


      decodeNet(DHCPConf _dhcp, String srvName) {
        for (String net in _dhcp.networks.keys) {
          for (String sub in _dhcp.networks[net].subNets.keys) {
            for (String hName in _dhcp.networks[net].subNets[sub].hosts.keys
                .toList()
              ..sort()) {
              HostDef host = new HostDef()
                ..net = srvName+"_"+net
                ..subNet = sub
                ..ip = _dhcp.networks[net].subNets[sub].hosts[hName].ip
                ..name = hName;
              if (!hosts.contains(host)) hosts.add(host);
              _hReader.addLocalHost(host.ip, host.name);
            }
          }
        }
      }

      List<String> getCNet(String ip) {
        List<String> _getCNet(DHCPConf _dhcp, String srvName) {
          for (String netName in _dhcp.networks.keys) {
            NetWork net = _dhcp.networks[netName];
            for (String subNet in net.subNets.keys) {
              String netIp = SubNet.applyMask(ip, net.subNets[subNet].netMask);
              if (netIp == net.subNets[subNet].ip) {
                //We have match of network
                return [srvName, netName, netIp];
              }
            }
          }
          return null;
        }

        List<String> ret = _getCNet(_dhcp_rtr, "router");
        if (ret == null) ret = _getCNet(_dhcp_fs, "flyfs");

        return ret;
      }

      //Find hostname for ip
      Future<HostDef> getHost(String ip) async {
        for (HostDef host in hosts)
          if (host.ip == ip) return host;

        //Host is not found!
        List<String> cNet = getCNet(ip);
        if (cNet != null) {
          HostDef crHost = new HostDef()
            ..name = _hReader.getName(ip)
            ..net = cNet[0]+"_"+cNet[1]
            ..ip = ip
            ..subNet = cNet[2];

          //String mac = "00:00:00:00:00:00"; //For now it will be only zeros
          if (!hosts.contains(crHost)) hosts.add(crHost); //Add to hosts
          DHCPConf _dhcp;
          if (cNet[0] == "router") _dhcp = _dhcp_rtr; //Get dhcp config
            else _dhcp = _dhcp_fs;
          String mac = await ShTools.arpGet(ip, cNet[0]);

          _dhcp.networks[cNet[1]].subNets[cNet[2]].hosts[crHost.name] =
          new Host(crHost.name, ip, mac);

          //Add to net

          return crHost;
        } else return new HostDef()
          ..name = _hReader.getName(ip)
          ..ip = ip; //Host is totally out of our ranges
      }

      Future<bool> decodeHosts(List<SHostDef> shosts) async {
        for (SHostDef sh in shosts) {
          HostDef host = await getHost(sh.ip);
          if (!hosts.contains(host)) hosts.add(host);
        }
        //Save decoded hosts
        await _dhcp_rtr.writeFile();
        await _dhcp_fs.writeFile();
        return true;
      }

      decodeNet(_dhcp_rtr, "router");
      decodeNet(_dhcp_fs, "flyfs");
      await _hReader.writeFile(); //Save new definitions
      await decodeHosts(_hReader.local);
      sortHosts(); //Sort hosts by ip address

      Map<String, dynamic> oMap = new Map<String, dynamic>();
      oMap["hosts"] = hosts.map((el) {
        if (el.net != null && el.net.isNotEmpty && el.net.indexOf("_") > -1) {
          NetPar nP = getSrv(el.net);

          return <String, dynamic>{
            "name": el.net + '-' + el.name + ' ' +
                nP.dhcp.networks[nP.netName].subNets[el.subNet].hosts[el.name].ip,
            "ip": nP.dhcp.networks[nP.netName].subNets[el.subNet].hosts[el.name].ip
          };
        } else {
          return <String, dynamic> {
            "name": "local"+"-"+el.name,
            "ip": el.ip
          };
        }
      }).toList();

      List<String> ips = (_sh.vars["OIPS"] as List<dynamic>).cast<String>();
      List<Map> redirected = new List(); //Init redirected list
      for (int i = 0; i < ips.length; i++) {
        String ip = ips[i];
        HostDef hd = await getHost(ip);
        redirected.add(<String, dynamic> {
          "name": (hd?.net ?? "")+'-'+(hd?.name ?? "")+' '+ip,
          "ip": ip,
          "ctr": i,
          "proto": _sh.vars["IPROTO"][i],
          "iport": _sh.vars["OPORTS"][i],
          "oport": _sh.vars["IPORTS"][i],
          "istcp": (_sh.vars["IPROTO"][i] == "tcp"),
          "isudp": (_sh.vars["IPROTO"][i] == "udp"),
        });
      }
      oMap["redirected"] = redirected;

      return oMap;
    });

    DataMaps.instance.setFunc("cHost", (Map<String, dynamic> opt) async {
      Map<String, dynamic> oMap = new Map<String, dynamic>();
      Host host;

      if ((opt["par"] == null) || (opt["par"] is String)) return <String, dynamic>{};
      //print(opt["par"]);
      oMap["hostSel"] = true;

      List<String> par = (opt["par"] as List<dynamic>).cast<String>();
      NetPar nP = getSrv(par[0]);

      String subName = par[1];
      String hostName = par[2]; //Decode parameters

      if (nP.netName.isEmpty) return <String, dynamic>{};
      if (subName.isEmpty) return <String, dynamic>{};

      if (hostName.isNotEmpty) {
        host = nP.dhcp.networks[nP.netName].subNets[subName].hosts[hostName];
        oMap["name"] = hostName;
        oMap["ip"] = host?.ip;
        oMap["mac"] = host?.mac;
      }

      oMap["freeIps"] = (nP.dhcp.networks[nP.netName].subNets[subName].availableIps().map((el) => {"ip": el}));

      //Priority and proxy is set ?
      oMap["prio"] = (getIdx("PRIO",host?.ip) > -1);
      oMap["proxy"] = (getIdx("PROXYADDR",host?.ip) > -1);

      int idx = -1;
      idx = getIdx("IPAREA1",host?.ip);
      if (idx > -1) { oMap["inet1"] = true; oMap["inet2"] = false; oMap["inetd"] = false; oMap["inetf"] = false; }
       else {
        idx = getIdx("IPAREA2", host?.ip);
        if (idx > -1) {
          oMap["inet1"] = false;
          oMap["inet2"] = true;
          oMap["inetd"] = false;
          oMap["inetf"] = false;
        } else {
        idx = getIdx("IPDYN", host?.ip);
        if (idx > -1) {
          oMap["inet1"] = false;
          oMap["inet2"] = false;
          oMap["inetd"] = true;
          oMap["inetf"] = false;
        } else {
        //idx is never found
        if (idx == -1) {
          oMap["inet1"] = false;
          oMap["inet2"] = false;
          oMap["inetd"] = false;
          oMap["inetf"] = true;
        }
        }
        }
      }
     return oMap;
    });
   }

    Future<String> renderNotFound([Map<String,String> par]) {
    DataMaps.instance.param = par;

    BasicRender render = new BasicRender('page404.yaml');
    return render.render;
  }

  Future saveForbidden(String data, { bool blockedLocal = false}) async {
    _hReader.saveForbidden(data);
    //Actualize ransomware too here
    await _hReader.updateRansomware();
    await _hReader.writeFile(blocked : blockedLocal);

    //Update mikrotik routers
    _hReader.updateMikrotik(mikrotiks).then((bool res) {
      if (res) LogData("All mikrotiks are updated successfully.");
    });
  }

  Future saveInternet(String data) async {
    _hReader.saveInternet(data);
    await _hReader.writeFile();
  }

  Future<bool> saveWatch(Map<String, dynamic> data) async {
    _watch.fillMap(data);
    await _watch.saveFile();
    return true;
  }

  Map<String, dynamic> getAllFails(DateTime startD, DateTime endD) =>
   _watch.getAllFails(startD, endD);

  Future saveGlobals(Map<String, dynamic> data) async {
    saveVar(String key) {
      String tmp = data[key] as String;
      tmp = (tmp ?? "").trim();
      if (tmp.isNotEmpty) _sh.vars[key] = tmp;
    }

    saveVar("IF1");saveVar("GW1");
    saveVar("IF2");saveVar("GW2");
    saveVar("SITE");saveVar("SRVIP");
    // target_xx, gate_xx, mask_xx
    // aport_xx, aproto_xx

    List<String> tgts = new List();
    List<String> gates = new List();

    List<String> aports = new List();
    List<String> aproto = new List();

    //Reset old mikrotik list
    mikrotiks = new List();

    for (String key in data.keys) {
      if (key.indexOf("target_") == 0) {
        String idx = key.substring(7);

        String tgt = (data[key] as String)+"/"+(data["tgt_mask_$idx"] as String);
        String gw = (data["gate_$idx"] as String);

        tgts.add(tgt);
        gates.add(gw);
      }



      if (key.indexOf("aport_") == 0) {
        String idx = key.substring(6);

        String port = (data[key] as String);
        String proto = (data["aproto_$idx"] as String);

        aports.add(port);
        aproto.add(proto);
      }

      if (key.indexOf("mikro_port_") == 0) {
        String idx = key.substring(11);

        String ip = (data["mikro_$idx"]) as String;
        String prt = (data["mikro_port_$idx"]) as String;
        String name = (data["mikro_name_$idx"]) as String;
        bool blocked = ((data["mikro_blocked_$idx"]) as String) == "X";

        int port = 0;
        try {
          port = int.parse(prt);
        } catch (err) {
          LogError(err.toString());
        }

        //print(ip+"->"+port.toString());
        mikrotiks.add(new Mikrotik(ip, port, name, blocked));

      }
    }

    //If parsed vals are not empty
    if (tgts.length > 0) {
      _sh.vars["RTARGETS"] = tgts;
      _sh.vars["RGATES"] = gates;
    }

    if (aports.length > 0) {
      _sh.vars["APORTS"] = aports;
      _sh.vars["APROTO"] = aproto;
    }

    await _sh.writeFile();
    await writeMikros();
    //print(data);
  }


  void serveNotFound(HttpRequest req) {
    renderNotFound(<String, String>{"path": req.uri.path}).then((String res) {
      String mimeType = 'text/html; charset=UTF-8';
      req.response.headers.set('Content-Type', mimeType);
      req.response.statusCode = HttpStatus.notFound;

      req.response.write(res);
      req.response.close();
    });
  }

  Future serveLogin(HttpRequest req, [int usrCheck]) async {
    if (usrCheck == null) usrCheck = 1; //If not defined then no error will be displayed
    DataMaps.instance.param = <String, dynamic>{"path": req.uri.path,
      "loginErr" : ((usrCheck == 0) || (usrCheck == 2)), "triesErr": (usrCheck == -1) };
    BasicRender render = new BasicRender('login.yaml');
    String res = await render.render;

    req.response.headers.set('Content-Type', 'text/html; charset=UTF-8');
    req.response.writeln(res);
    req.response.close();
  }

  Future servePath(HttpRequest req, String path, [dynamic par]) async {
    String user = req.session["username"];
    //Check username
    if (user != 'admin'&& path != 'tickets') req.response.redirect(Uri.parse(ticketsUrl.toString()));
     else {
      DataMaps.instance.param =
      <String, dynamic>{"path": req.uri.path, "par": par};
      BasicRender render = new BasicRender(path + '.yaml');
      String res = await render.render;

      req.response.headers.set('Content-Type', 'text/html; charset=UTF-8');
      req.response.writeln(res);
      req.response.close();
    }
  }

  Future<String> delRedirUrl(String ip, String oport) async {
    if (ip.isNotEmpty) {
      prtDelVar(ip, oport);
      await _sh.writeFile();
      return "OK";
    }
    else return "Ip is empty!";
  }

  Future<String> editRedirUrl(String ip, String oport, String iport, String proto, String hName) async {
    if (ip.isNotEmpty) {
      if (ip == "new") return "Хоста е невалиден!";
      //Find index
      int usedIdx = -1;
      List<dynamic> tmp = _sh.vars["IPORTS"] as List<dynamic>;
      int i = 0;
      do {
        //print("$i:"+tmp[i]+"-"+oport);
        if (tmp[i] == oport) usedIdx = i; //Found the port
        i++;
      } while (i < tmp.length && (tmp[i] != oport || _sh.vars["OIPS"][i] != ip));
      if (i < tmp.length) usedIdx = i; //Index is found

      int idx = -1;
      if (usedIdx > -1 && _sh.vars["OIPS"][usedIdx] == ip) idx = usedIdx;
      else
        //It's not our index so find the idx we are looking for
        idx = (_sh.vars["OIPS"] as List<dynamic>).cast<String>().indexOf(ip);

      //print("$usedIdx/$idx $ip $oport $iport $proto");

      //Add to hosts file
      _hReader.addLocalHost(ip, hName);

      //Same port is used on same protocol and it's not our record
      if ((usedIdx > -1) && (idx != usedIdx) && (_sh.vars["IPROTO"][usedIdx] == proto)) return "Порта е вече зает!";

      //Same ip different port number -> then will be new port forward
      if (idx > -1 && _sh.vars["IPORTS"][idx] != oport) idx = -1;

      if (idx == -1) {
        //Adding new port mapping
        _sh.vars["OIPS"].add(ip);
        _sh.vars["IPORTS"].add(oport);
        _sh.vars["OPORTS"].add(iport);
        _sh.vars["IPROTO"].add(proto);
      } else {
        //Already exists just change port numbers
        // _prt.vars["oport"][idx] = oport; -> Outport shoudn't be modified we are comparing trough it
        //_sh.vars["IPORTS"][idx] = oport;
        _sh.vars["OPORTS"][idx] = iport;
        _sh.vars["IPROTO"][idx] = proto;
      }

      LogData('Port forward edit: $ip<$oport -> $iport / $proto>');

      await _sh.writeFile();//Write changes back
      return "OK";
    } return "Хоста е празен!";
  }


  void delPrioArea(String ip) {
    //Priority and internet choose
    const keys = const ["PRIO", "IPAREA1", "IPAREA2", "IPDYN", "PROXYADDR"];
    for (String key in keys) {
      int idx = getIdx(key, ip);
      if (idx > -1) (_sh.vars[key] as List<dynamic>).cast<String>().removeAt(idx);
    }
  }


  Future editHost(HttpRequest req, List param, Map data) async {
    Host host;
    NetPar nP = getSrv(param[0] as String);

    if ((param[2] as String).isNotEmpty) {
        host = nP.dhcp.networks[nP.netName].subNets[param[1]].hosts[param[2]];

        //Change port forwarding ip into place
        prtChgVar(host.ip, data["ipval"] as String);
        _hReader.delLocal(host.ip); //Remove old ip

        host.name = data["nameval"] as String;
        host.ip = data["ipval"] as String;
        host.mac = (data["macval"] as String).replaceAll("-", ":");

        //Remove old name
        nP.dhcp.networks[nP.netName].subNets[param[1]].hosts.remove(param[2]);
    } else host = new Host(data["nameval"] as String, data["ipval"] as String, data["macval"] as String);

    //Add with new name
    nP.dhcp.networks[nP.netName].subNets[param[1]].hosts[host.name] = host;

    _hReader.addLocalHost(host.ip, host.name);
    //Remove old entries
    delPrioArea(host.ip);

    //Add new entries
    if (data["prio"] != null) (_sh.vars["PRIO"] as List<dynamic>).cast<String>().add(host.ip);
    if (data["proxy"] != null) (_sh.vars["PROXYADDR"] as List<dynamic>).cast<String>().add(host.ip);
    switch (data["inet"] as String) {
      case "1": (_sh.vars["IPAREA1"] as List<dynamic>).cast<String>().add(host.ip);
       break;
      case "2": (_sh.vars["IPAREA2"] as List<dynamic>).cast<String>().add(host.ip);
       break;
      case "3": (_sh.vars["IPDYN"] as List<dynamic>).cast<String>().add(host.ip);
    }


    //Write file back
    await _hReader.writeFile();
    await _sh.writeFile();
    await nP.dhcp.writeFile();
    //await _sh.writeFile();

    servePath(req, 'layout');
  }

  void serveStatic(HttpRequest req, {String path}) {
    String MY_HTTP_ROOT_PATH;
    if (path == null) {
      path = req.uri.path; // if called with no path specified;
      if (path.indexOf('/ipconf') == 0) path = path.substring(7);
      MY_HTTP_ROOT_PATH = Platform.script.resolve(templatePath).toFilePath();
    } else MY_HTTP_ROOT_PATH = Platform.script.resolve(templatePath).toFilePath();
    //Start with current path if path is put by parameter

    File file = new File(MY_HTTP_ROOT_PATH + path);
    file.exists().then((bool found) {
      String mimeType;
      try {
        mimeType = Mime.mime(path);
      } catch (err) { }
      if (mimeType == null) mimeType = 'text/plain; charset=UTF-8';
      req.response.headers.set('Content-Type', mimeType);
      if (found) {
        file.openRead()
            .pipe(req.response) // HttpResponse type.
            .catchError((dynamic e) => LogError(e.toString()));
      } else {
        LogData("Content required was not found: $path");
        serveNotFound(req);
      }
    });
  }

  void prtChgVar(String oldIp, String newIp) {
    dynamic el = _sh.vars["OIPS"];
    if (el is String) el = new List<String>()..add(el as String);
    if (el is List) {
      int i = el.indexOf(oldIp);
      if (i > -1) {
        el[i] = newIp; //Change ip into place keeping all other vars the same
        _sh.vars['OIPS'] = el;
      }
    }
  }

  void prtDelVar(String ip, String oport) {
    dynamic el = _sh.vars["OIPS"];
    if (el is String) el = new List<String>()..add(el as String);
    if (el is List) {
      for (int i = 0; i < el.length; i++) {
        //Delete from ports if oport == -1 or ip // oport are suitable
        if (el[i] == ip && (oport == "-1" || _sh.vars['IPORTS'][i] == oport)) {
          el.removeAt(i);
          _sh.vars['OIPS'] = el;
          _sh.vars['IPORTS'].removeAt(i);
          _sh.vars['OPORTS'].removeAt(i);
          _sh.vars['IPROTO'].removeAt(i);
        }
       }
     }
    }

  Future delHost(List par) async {
    NetPar nPar = getSrv(par[0] as String);

    Host host = nPar.dhcp.networks[nPar.netName].subNets[par[1]].hosts[par[2]];
    nPar.dhcp.networks[nPar.netName].subNets[par[1]].delHost(par[2] as String);
    prtDelVar(host.ip, "-1");
    delPrioArea(host.ip); //Delete priorities and internet choice if any
    _hReader.delLocal(host.ip); //Delete a local host

    await _hReader.writeFile();
    await nPar.dhcp.writeFile();
    await _sh.writeFile();
  }

  Future<Null> loadMikros() async {
    //Load mikrotik list
    try {
      String rFile = await(new File(mikroFile).readAsString());
      Map<String, dynamic> decFile = Yaml.readYaml(rFile);
      List<Map> mikroList = [];
      for(var name in decFile.keys) {
        decFile[name]['name'] = name; //Set name
        mikroList.add(decFile[name]); //Convert to list
      }

      mikroList.sort((a,b) => a['name'].compareTo(b['name']));
      mikrotiks = mikroList.map((Map el) => new Mikrotik.fromMap(el)).toList();
    } catch (err) {
      LogError(err.toString());
      mikrotiks = [];
    }

    if (mikrotiks == null || mikrotiks.isEmpty)
      try {
        String rFile = await(new File(mikroFile.replaceAll('.yaml', '.json')).readAsString());
       List<Map> decFile = (json.decode(rFile) as List<dynamic>).cast<Map>();
       mikrotiks = decFile.map((Map el) => new Mikrotik.fromMap(el)).toList();
      } catch(err) {
       LogError(err.toString());
       mikrotiks = [];
     }
  }

  Future<Null> writeMikros() async {
    List<Map<String,dynamic>> mikroList = mikrotiks.map((el) => el.toMap()).toList();
    Map<String,dynamic> oMap = {};
    for (var el in mikroList) {
      String name = el['name'];
      if (name == null || name.isEmpty) name = el['ip']; //Set ip address as name on empty name
      oMap[name] = el..remove('name');
    }
    String outS = Yaml.writeYaml(oMap);

    try {
      await new File(mikroFile).writeAsString(outS);
    } catch(err) {
      LogError(err.toString());
    }
  }

  Future loadSettings() async {
    //Init settings
    _dhcp_rtr = new DHCPConf(_conf.params["dhcp_rtr"] as String);
    await _dhcp_rtr.readFile();
    _dhcp_fs = new DHCPConf(_conf.params["dhcp_fs"] as String);
    await _dhcp_fs.readFile();
    //print(_dhcp_rtr.networks["eth0"].subNets.keys);

    await loadMikros();

    //init shell script reader
    _sh = new SHReader(_conf.params["rmulti"] as String);
    await _sh.readFile();

    //Init host reader
    _hReader = new HostReader();
    await _hReader.readFile();

    //Load internet watcher
    _watch = new Watch(_hReader, this);
    await _watch.loadFile();

    //Init file if needed
    _sh.vars["OIPS"] = _sh.vars["OIPS"] ?? new List<String>();
    _sh.vars["IPORTS"] = _sh.vars["IPORTS"] ?? new List<String>();
    _sh.vars["OPORTS"] = _sh.vars["OPORTS"] ?? new List<String>();
    _sh.vars["IPROTO"] = _sh.vars["IPROTO"] ?? new List<String>();
  }

  RenderPages(this._conf) {
    setFuncs();
    loadSettings();
  }

}