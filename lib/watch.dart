library Watch;

import 'dart:io';
import 'dart:async';
import 'dart:convert';
import 'package:tools/logger.dart';
import 'package:tools/tools.dart';

import 'shTools.dart';
import 'probe.dart';
import 'hostReader.dart';
import 'render.dart';

class WFail {
  DateTime fTime;
  int state;

  WFail(this.fTime, this.state);
  WFail.fromList(List el) {
    fTime = el[0] as DateTime;
    state = el[1] as int;
  }

  List toList() {
    return <dynamic>[fTime, state];
  }

  List toListEnc() {
    return <dynamic>[Dates.bgDateFull(fTime.toString()), state];
  }
}

class WFailes {
  static const int keepOld = 30; //Days
  static const fName = "../log/fails.dat";

  List<WFail> _fails;
  Future<Null> addFail(WFail f) async {
    if (_fails == null) _fails = new List(); //Init list if necesarry
    _fails.add(f);
    clearOld();
    await writeFile();
  }

  Future<Null> setFailes(List<WFail> wfails) async { //Testing purposes only
    _fails = wfails;
    await writeFile();
  }

  List<WFail> getFails(DateTime startD, DateTime endD) {
     List<WFail> outL = new List();
     for (int i = 0; i < _fails.length; i++)
       if (_fails[i].fTime.isAfter(startD) &&
           _fails[i].fTime.isBefore(endD)) outL.add(_fails[i]);

     return outL;
  }
  
  List<List<dynamic>> percentFails(List<WFail> ourFails, DateTime startD, DateTime endD) {
    List<List<dynamic>> res = new List();
    int sTime = startD.millisecondsSinceEpoch;
    int eTime = endD.millisecondsSinceEpoch;
    int interval = eTime - sTime; //Time interval (100% of our time)
    
    for (int i = 0; i < ourFails.length; i++) {
      int ourTime = ourFails[i].fTime.millisecondsSinceEpoch - sTime; //Our interval
      res.add(<dynamic>[(ourTime/interval)*100, ourFails[i].state]); //[(PercentOfTimeInterval),(state)]
    }

    return res;
  }

  //Get fails and calculate them
  List<List<dynamic>> getPFails(DateTime startD, DateTime endD) {
    List<WFail> ourFails = getFails(startD, endD);
    return percentFails(ourFails, startD, endD);
  }

  Map<String, dynamic> getAllFails(DateTime startD, DateTime endD) {
    List<WFail> ourFails = getFails(startD, endD);
    List<List<dynamic>>  percFails = percentFails(ourFails, startD, endD);
    return <String, dynamic> {
      "log": ourFails.map((WFail fail) => fail.toListEnc()).toList(),
      "perc": percFails
    };
  }

  void clearOld() {
    int i = 0;
    DateTime dNow = new DateTime.now();
    while (i < _fails.length) {
      int dur = dNow.difference(_fails[i].fTime).inDays;
      if (dur > keepOld) _fails.removeAt(i); //No need to increase i element is deleted and elements are shifted
       else i++; //Increase i if element is not removed
    }
  }

 Future<bool> readFile() async {
    List<int> data;
    try {
      data = await new File(fName).readAsBytes();
    } catch(err) {
      LogError(err.toString());
    }

    if (data == null) {
      _fails = new List();
      return false;
    } //Start over if file is not present

    //Convert from List<List<dynamic>>
    _fails = (BinTools.decode(data) as List<dynamic>).cast<List<dynamic>>()
        .map((List<dynamic> el) => new WFail.fromList(el)).toList();

   return true;
 }


 Future<bool> writeFile() async {
    bool isOk = true;
    //Convert to List<List<dynamic>>
    List<int> data = BinTools.encode(
        _fails.map((WFail el) => el.toList()).toList());
    try {
      await new File(fName).writeAsBytes(data);
    } catch(err) {
      LogError(err.toString());
      isOk = false;
    }

    return isOk;
 }

}

class Watch {
  static const inFile = '../conf/routes.yaml';
  static const List<String> routes = const <String>['r1', 'r2', 'rm'];
  static const List<String> routesRm = const <String>['r1', 'r2','rmulti'];
  static const Duration wTime = const Duration(minutes: 1); //Watch time 1 minute
  static const Duration rTime = const Duration(days: 2); //Watch time 2 days -> ransomware update

  int cRoute = 2;

  bool _rWatch = true;
  bool get rWatch => _rWatch;
  Timer _wTimer, _rTimer;
  Probe _probe;
  WFailes _fails;
  HostReader _hReader;
  RenderPages _rPages;
  List<Mikrotik> get mikrotiks => _rPages.mikrotiks;

  void set rWatch(bool state) {
    _rWatch = state;
    if (state) initWatcher();
     else stopWatcher();
  }

  Future<bool> allProbes() async {
    List<bool> probes = (await _probe.probeAll()).values.toList();
    bool res = false;
    for (int i = 0; i < probes.length; i++)
    if (probes[i]) res = true;

    return res;
  }

  void initWatcher() {
    //Ransomware update timer if hostreader is not null
    if (_rTimer == null && _hReader != null) {
      _rTimer = new Timer.periodic(rTime, (Timer rT) async {
        await _hReader.updateRansomware(); //Update ransomware list
        String path = "../sh";
        //Will execute here
        try {
          //Host file update ransomware
          ProcessResult res = await Process.run(path+"/hostUpdate.sh",[],workingDirectory: path);
          LogData("Ransomware HostUpdate:"+res.stderr.toString()); //Info comes in error for this tool
          LogData("Ransomware HostUpdate:"+res.stdout.toString());

          //Update mikrotik ips
          _hReader.updateMikrotik(mikrotiks);
        } catch (err) { LogError("ERROR:"+err.toString()); }
      });
    }

    if (_wTimer == null) _wTimer = new Timer.periodic(wTime , (Timer wT) async {
      //Gate 1
      await ShTools.run('/home/scripts/rmulti.sh gw1', 'router'); //Set router gateway to GW1
      bool r1 = await allProbes();

      //Gate 2
      await ShTools.run('/home/scripts/rmulti.sh gw2', 'router'); //Set router gateway to GW2
      bool r2 = await allProbes();

      int status = -1;
      if (!r1 && r2) status = 0; //Inet 1 is failed
      if (r1 && !r2) status = 1; //Inet 2 is failed
      if (!r1 && !r2) status = 3; //Inet1 and Inet2 are both failed
      if (status != -1) LogData("Internet failing! Status $status");

      if (status > -1)  //Add failed attempt
        await _fails.addFail(new WFail(new DateTime.now(), status));

      //Set distinguished route if all internets are present
      if (status == -1) await ShTools.run('/home/scripts/rmulti.sh '+routesRm[cRoute], 'router');

      //Set specific internet when only it is present
      if (status == 0) await ShTools.run('/home/scripts/rmulti.sh r2', 'router');
      if (status == 1) await ShTools.run('/home/scripts/rmulti.sh r1', 'router');
    });
  }

  void stopWatcher() {
    //Remove timer
    if (_wTimer != null) {
      _wTimer.cancel();
      _wTimer = null;
    }

    if (_rTimer != null) {
      _rTimer.cancel();
      _rTimer = null;
    }
  }

  Watch(this._hReader, this._rPages) {
    //Set probe addresses
    _probe = new Probe()
         ..setTarget(0, "8.8.8.8") //Google1
         ..setTarget(1, "8.8.4.4"); //Google2
//         ..setTarget(2, "208.67.222.222") //OpenDNS
//         ..setTarget(3, "208.67.220.220"); //OpenDNS

    rWatch = true; //Init watcher
  }

  Map toMap() =>
      <String, dynamic>{"route": routes[cRoute],
       "watch": rWatch };

  void fillMap(Map<String, dynamic> inMap) {
    cRoute = routes.indexOf(inMap["route"] as String);
    rWatch = inMap["watch"] as bool;
  }

  void setProbes(List<WFail> wfails) { //Testing purposes only
    _fails.setFailes(wfails);
  }

  //Remap from fails
  List<WFail> getFails(DateTime startD, DateTime endD) =>
    _fails.getFails(startD, endD);
  List<List<dynamic>> getPercent(DateTime startD, DateTime endD) =>
    _fails.getPFails(startD, endD);
  Map<String, dynamic> getAllFails(DateTime startD, DateTime endD) =>
    _fails.getAllFails(startD, endD);

  Future<bool> loadFile() async {
    Map<String, dynamic> pData;

    try {
      String data = await new File(inFile).readAsString();
      pData = Yaml.readYaml(data);
    } catch (err) {
      LogError(err.toString());
    }

    if (pData == null) {
      try {
        String data = await new File(inFile.replaceAll('.yaml', '.json')).readAsString();
        pData = json.decode(data) as Map<String, dynamic>;
      } catch (err) {
        LogError(err.toString());
      }
    }

    if (_fails == null) _fails = new WFailes();
    //Load failed attempts
    await _fails.readFile();

    if (pData == null) return false;
    fillMap(pData);
    return true;
  }

  Future<bool> saveFile() async {
    //Apply watch
    await ShTools.run('/home/scripts/rmulti.sh '+routesRm[cRoute], 'router');

    //Save to file
    Map pData = toMap();
    bool isOk = true;
    try {
      await new File(inFile).writeAsString(Yaml.writeYaml(pData));
    } catch (err) {
      isOk = false;
      LogError(err.toString());
    }

    return isOk;
  }

  String toString() => toMap().toString();
}