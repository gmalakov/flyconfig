library server;

import 'dart:io';
import 'dart:async';
import 'dart:convert';

import 'package:route/server.dart';
import 'package:route/pattern.dart';
import 'package:hostconf/logins.dart';
import 'package:tools/conf_reader.dart';
import 'package:tools/logger.dart';
import 'package:tools/tools.dart';
import 'package:http_server/http_server.dart';

import 'urls.dart';
import 'render.dart';
import 'shTools.dart';
import 'ipt_analyzer.dart';

class ConfServer {
  Timer autoApply;
  static const Duration applyTime = const Duration(minutes: 3); //3minutes

  ConfReader _conf;
  RenderPages _rPages;
  Logins _log = new Logins()
   ..loadUsers();

  String bodyGet(List<List<int>> input) {
    String res = '';
    for (int i = 0; i < input.length; i++) {
      res += utf8.decode(input[i], allowMalformed: true);
    }

    return res;
  }


  void setTime() {
    if (autoApply != null) autoApply.cancel();
    autoApply = new Timer(applyTime, () {
      autoApply.cancel();
      autoApply = null;
      LogData("Auto applying settings (timer) ...");
      applySettings();
    });
  }

  void clearTime() {
    if (autoApply != null) autoApply.cancel();
    autoApply = null;
  }

  Future applySettings() async {
    String path = "../sh";
    //Will execute here
    try {
      ProcessResult res = await Process.run(path+"/apply.sh",[],workingDirectory: path);
      LogData("APPLY:"+res.stderr.toString()); //Info comes in error for this tool
      LogData("APPLY:"+res.stdout.toString());
    } catch (err) { LogError("ERROR:"+err.toString()); }
  }


  void serveHome(HttpRequest req) {
    _rPages.servePath(req,'layout');
  }

  void redir(HttpRequest req) {
    _rPages.servePath(req,'ports');
  }

  void forbidden(HttpRequest req) {
    _rPages.servePath(req, 'forbidden');
  }

  void tickets(HttpRequest req) {
    _rPages.servePath(req, 'tickets');
  }

  void inetHosts(HttpRequest req) {
    _rPages.servePath(req, 'inethosts');
  }

  void watch(HttpRequest req) {
    _rPages.servePath(req,'watch');
  }

  void globals(HttpRequest req) {
    _rPages.servePath(req, 'globals');
  }

  void serveEdit(HttpRequest req) {
    //print(req.uri.queryParameters);
    var param = editHostUrl.parse(req.uri.path);
    _rPages.servePath(req,'layout',param);
  }

  Future serveSave(HttpRequest req) async {
    var param = editHostUrl.parse(req.uri.path);
    HttpBodyHandler.processRequest(req).then((HttpBody body) {
      Map data = body.body as Map;
      LogData("User "+_log.checkLogin(req.session["sesId"+reqIp(req)] as String)+
          ": Saving host "+param.toString()+" ->\n "+
          data.toString());
      _rPages.editHost(req, param, data);
      setTime();
    });
  }

  String reqIp(HttpRequest req) {
    List<String> h = req.headers["X-Real-IP"];
    String ip;
    if (h == null) ip = req.connectionInfo.remoteAddress.address;
    else ip = h[0];

    return ip;
  }

  void login(HttpRequest req, [int usrCheck]) => _rPages.serveLogin(req, usrCheck);

  Future doLogin(HttpRequest req) async {
    HttpBodyHandler.processRequest(req).then((HttpBody body) {
      Map data = body.body as Map;
      int usrCheck = _log.checkUser(data["user"] as String, data["pass"] as String, reqIp(req));
      if (usrCheck == 1) {
        String sesId = _log.addLogin(data["user"] as String);
        req.session["sesId"+reqIp(req)] = sesId;
        //Store username! -> won't be used for username check
        req.session["username"] = data["user"];
        req.response.redirect(Uri.parse(homeUrl.toString()));
        LogData("User login "+(data["user"] as String)+" address "+reqIp(req));
      } else {
        //_log.addUser("admin", "Admin adminov", "maniac1234");
        //_log.saveUsers();
        login(req, usrCheck);

        if (usrCheck == 2) LogData("User:"+(data["user"] as String)+" doesn't exist. address "+reqIp(req));
        if (usrCheck == 0) LogData("Invalid login for user: "+(data["user"] as String)+" address "+reqIp(req));
        if (usrCheck == -1) LogData("Maximum tries time reached for IP:."+reqIp(req));
      }
    });
  }

  Future<bool> authFilter(HttpRequest req) async {
    String username = _log.checkLogin(req.session["sesId"+reqIp(req)] as String);
    if (username != null) return true;
     else {
       loginRedir(req);
       return false;
    }
  }

  void loginRedir(HttpRequest req) {
    req.response.redirect(Uri.parse(loginUrl.toString()));
  }

  Future newPassword(HttpRequest req) async {
    String user = _log.checkLogin(req.session["sesId"+reqIp(req)] as String);
    var param = newPasswordUrl.parse(req.uri.path);
    if (param.length < 2) {
      LogData("User $user, address "+reqIp(req)+" password modify invalid parameter count!");
      req.response.write("Невалиден брой параметри!");
      await req.response.close();
     return;
    }

      String oldPass = param[0];
      String newPass = param[1];

      int usrCheck = _log.checkUser(user, oldPass, reqIp(req));
      if (usrCheck == 1) {
        _log.addUser(user, '', newPass);
        await _log.saveUsers();
        LogData("User $user, address "+reqIp(req)+" new passsword $newPass.");
        req.response.write("Успешно променена парола.");
        await req.response.close();
      } else {
        LogData("User $user, address "+reqIp(req)+" try to modify passsword with $newPass.");
        req.response.write("Проблем при промяна на паролата!");
        await req.response.close();
      }
   }

  void doLogout(HttpRequest req) {
    String user = _log.checkLogin(req.session["sesId"+reqIp(req)] as String);
    LogData("User $user logged out. Address "+reqIp(req));
    _log.delLogin(req.session["sesId"+reqIp(req)] as String ?? "");
    loginRedir(req);
  }

  Future doReload(HttpRequest req) async {
    await _rPages.loadSettings();
    req.response.redirect(Uri.parse(homeUrl.toString()));
  }

  Future applySets(HttpRequest req) async {
    LogData("User "+_log.checkLogin(req.session["sesId"+reqIp(req)] as String)+
        ": Applying host settings.");
    clearTime(); //Clear timer --> settings are applied
    applySettings();
    req.response.redirect(Uri.parse(homeUrl.toString()));
  }

  void delHost(HttpRequest req) {
    var param = delHostUrl.parse(req.uri.path);
    _rPages.delHost(param);
    LogData("User "+_log.checkLogin(req.session["sesId"+reqIp(req)] as String)+
        ": Deleting host "+param.toString());
    req.response.redirect(Uri.parse(homeUrl.toString()));
  }

  Future newTicket(HttpRequest req) async {
    var param = newTicketUrl.parse(req.uri.path);

    LogData("User "+_log.checkLogin(req.session["sesId"+reqIp(req)] as String)+
        ": Adding ticket ->"+param[0].toString()+" for "+param[1].toString()+
        " minutes");

    int minutes = 10; //Default minutes
    try { minutes = int.parse(param[1].toString()); } catch (err) {}

    //Add ticket to this ip for minutes time
    Tickets.instance.addTicket(param[0], minutes);

    req.response.write("Added");
    req.response.close();
  }

  Future delTicket(HttpRequest req) async {
   var param = delTicketUrl.parse(req.uri.path);

   LogData("User "+_log.checkLogin(req.session["sesId"+reqIp(req)] as String)+
       ": Deleting ticket ->"+param[0].toString());

    //Delete ticket and ip -> Ip rule will be deleted automaticaly by stream listener
    Tickets.instance.delTicket(param[0]);

    req.response.write("OK");
    req.response.close();
  }

  Future editRedir(HttpRequest req) async {
    var param = editRedirUrl.parse(req.uri.path);
    LogData("User "+_log.checkLogin(req.session["sesId"+reqIp(req)] as String)+
        ": Adding port redirection. ->"+param.toString());
    req.response.write(await _rPages.editRedirUrl(param[0], param[1], param[2], param[3], param[4]));
    req.response.close();
    setTime(); //Schedule apply settings
  }

  Future delRedir(HttpRequest req) async {
    var param = delRedirUrl.parse(req.uri.path);
    LogData("User "+_log.checkLogin(req.session["sesId"+reqIp(req)] as String)+
        ": Deleting port redirection. ->"+param.toString());
    req.response.write(await _rPages.delRedirUrl(param[0], param[1]));
    req.response.close();
    setTime(); //Schedule apply settings
  }


  Future saveForbidden(HttpRequest req) async {
    Map<String, dynamic> data;
    try {
      String sdata = bodyGet(await req.toList());
      data = (json.decode(sdata) as Map).cast<String, dynamic>();
    } catch (err) { LogError(err.toString()); }
    if (data != null) {
      LogData("User " +
          _log.checkLogin(req.session["sesId" + reqIp(req)] as String) +
          ": Saving forbidden addresses.");
      await _rPages.saveForbidden(data['forbidden'],
          blockedLocal: data['applyLocal'] as bool);

      //Apply blocked addresses localy ??
      _conf.params["apply_blocked_locally"] = data['applyLocal'];
      _conf.writeFile();

      req.response.write("saved");
      req.response.close();
      //setTime(); //Schedule apply settings
    }
  }

  Future saveInet(HttpRequest req) async {
    String data;
    try {
      data = bodyGet(await req.toList());
    } catch (err) { LogError(err.toString()); }

    LogData("User "+_log.checkLogin(req.session["sesId"+reqIp(req)] as String)+
        ": Saving internet addresses.");
    await _rPages.saveInternet(data);
    req.response.write("saved");
    req.response.close();
    setTime(); //Schedule apply settings
  }

  Future arpGet(HttpRequest req) async {
    Map<String, dynamic> data;
    try {
      String sdata = bodyGet(await req.toList());
      data = json.decode(sdata) as Map<String, dynamic>;
    } catch (err) {
      LogError(err.toString());
    }

    String mac = await ShTools.arpGet(data["ip"] as String, data["net"] as String);
    req.response.write(mac.toUpperCase()); //Write result back (UPCASE string)
    req.response.close();
  }

  Future getWatchInterval(HttpRequest req) async {
    Map<String, dynamic> data;
    try {
      String sdata = bodyGet(await req.toList());
      data = json.decode(sdata) as Map<String, dynamic>;
    } catch (err) {
      LogError(err.toString());
    }

    DateTime startD = Dates.parseDate(Dates.engDateFull(data["startD"] as String));
    DateTime endD = Dates.parseDate(Dates.engDateFull(data["endD"] as String));
    Map<String, dynamic> res = _rPages.getAllFails(startD, endD);
    String sRes = "";
    try {
      sRes = json.encode(res);
    } catch(err) {
      LogError(err.toString()+' Encoding ->'+res.toString());
    }

    //print(sRes);
    req.response.write(sRes); //Encode data to json
    req.response.close();
  }

  Future saveWatch(HttpRequest req) async {
    Map<String, dynamic> data;
    try {
      String sdata = bodyGet(await req.toList());
      data = json.decode(sdata) as Map<String, dynamic>;
    } catch (err) {
      LogError(err.toString());
    }

    LogData("User "+_log.checkLogin(req.session["sesId"+reqIp(req)] as String)+
        ": Saving watcher preferences.");
    await _rPages.saveWatch(data);

    req.response.write("saved");
    req.response.close();
  }

  Future saveGlobals(HttpRequest req) async {
    Map<String, dynamic> data;
    try {
      String sdata = bodyGet(await req.toList());
      data = (json.decode(sdata) as Map).cast<String, dynamic>();
    } catch (err) {
      LogError(err.toString());
    }

    _rPages.saveGlobals(data);
    //Do something here
    //String mac = await ShTools.arpGet(data["ip"], data["net"]);

    LogData("User "+_log.checkLogin(req.session["sesId"+reqIp(req)] as String)+
        ": Saving global addresses and preferences.");
    req.response.write("saved"); //Write result back (UPCASE string)
    req.response.close();
  }

  ConfServer(this._conf) {
    _rPages = new RenderPages(_conf);

    HttpServer.bind(_conf.params["host"], _conf.params["port"] as int).then((server) {
      new Router(server)
        ..filter(matchAny(authUrl), authFilter)
        ..serve(homeUrl, method: 'GET').listen(serveHome)
        ..serve(editHostUrl, method: 'GET').listen(serveEdit)
        ..serve(editHostUrl, method: 'POST').listen(serveSave)
        ..serve(loginUrl, method: 'GET').listen(login)
        ..serve(loginUrl, method: 'POST').listen(doLogin)
        ..serve(logoutUrl,method: 'GET').listen(doLogout)
        ..serve(reloadUrl, method: 'GET').listen(doReload)
        ..serve(applyUrl, method: 'GET').listen(applySets)
        ..serve(delHostUrl, method: 'GET').listen(delHost)

        ..serve(ticketsUrl, method: 'GET').listen(tickets)
        ..serve(newTicketUrl, method: 'POST').listen(newTicket)
        ..serve(delTicketUrl, method: 'POST').listen(delTicket)

        ..serve(forbiddenUrl, method: 'GET').listen(forbidden)
        ..serve(saveForbiddenUrl, method: 'POST').listen(saveForbidden)

        ..serve(inetHostsUrl, method: 'GET').listen(inetHosts)
        ..serve(saveInetUrl, method: 'POST').listen(saveInet)

        ..serve(watchUrl, method: 'GET').listen(watch)
        ..serve(saveWatchUrl, method: 'POST').listen(saveWatch)
        ..serve(getWatchIntervalUrl, method: 'POST').listen(getWatchInterval)

        ..serve(globalsUrl, method: 'GET').listen(globals)
        ..serve(saveGlobalsUrl, method: 'POST').listen(saveGlobals)

        ..serve(redirUrl, method: 'GET').listen(redir)
        ..serve(editRedirUrl, method: 'POST').listen(editRedir)
        ..serve(delRedirUrl, method: 'POST').listen(delRedir)

        ..serve(newPasswordUrl, method: 'POST').listen(newPassword)

        ..serve(arpget, method: 'POST').listen(arpGet)
        ..serve(matchAny(statUrls)).listen(_rPages.serveStatic)

        ..defaultStream.listen(_rPages.serveNotFound);
    });
    //Logger.console = true;

    //Add ip remove to ticket stream
    Tickets.instance.onDelTicket.listen((String ip) async {
      await IptAnalyzer.delHost(ip);
      LogData("Ticket for $ip has been deleted.");
    });

    Tickets.instance.onAddTicket.listen((String ip) => IptAnalyzer.addHost(ip));
  }


}