library shTools;

import 'dart:io';
import 'dart:async';
import 'package:tools/logger.dart';

class ShTools {
  static const path='../sh';

  static String trimRes(String inS, {bool trim = true}) {
    String res = inS.trim().replaceAll("\t", " ");
    res = res.replaceAll("\r", "\n");
    if (trim) {
      res = res.replaceAll("\n", " ");
    }
    return res;
  }

  static Future<String> arpGet(String ip, String srvName) async {
    String res;
    try {
      ProcessResult pR = await Process.run(
          path + "/dossh.sh", ['arpget', srvName, ip], workingDirectory: path);
      res = trimRes(pR.stdout.toString());
      String errS = pR.stderr.toString();
      if (errS.isNotEmpty) LogError(errS);
    } catch (err) {
      LogError(err.toString());
    }

    List<String> lRes = res.split(" ");
    if (lRes.length < 2 || lRes[1] != "ether") return "00:00:00:00:00:00";
    return lRes[2]; //Mac address
  }

  static Future<String> run(String cmd, String srvName, {bool trim = true}) async {
    String res;
    try {
      ProcessResult pR = await Process.run(
          path + "/dossh.sh", ['run', srvName, cmd], workingDirectory: path);

      res = trimRes(pR.stdout.toString(), trim: trim);

      String errS = pR.stderr.toString();
      if (errS.isNotEmpty) LogError(errS);
    } catch (err) {
      LogError(err.toString());
    }

    return res;
  }

  static Future<String> applyMikro(String srvName, int port, String fName) async {
    const Duration limitTime = const Duration(minutes: 2);

    String res;
    try {
      ProcessResult pR = await Process.run(
          "bash", ['-c',"ssh -p "+port.toString()+" admin@"+srvName+" < "+fName]).timeout(limitTime); //, workingDirectory: path
      res = trimRes(pR.stdout.toString());
      String errS = pR.stderr.toString();
      if (errS.isNotEmpty) LogError(errS);
    } catch (err) {
      LogError(err.toString());
    }

    return res;
  }

}