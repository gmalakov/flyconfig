library hostReader;

import 'dart:io';
import 'dart:async';
import 'package:tools/logger.dart';
import 'package:tools/httpGet.dart' as http;
import 'shTools.dart';

class Mikrotik {
  String ip;
  int port;
  String name;
  bool blockedSites = false;

  Map<String, dynamic> toMap() => {
    'ip': ip,
    'port': port,
    'name': name,
    'blocked': blockedSites
  };

  Mikrotik(this.ip, this.port, this.name, this.blockedSites);

  Mikrotik.fromMap(Map el) {
    ip = el["ip"] as String;
    port = el["port"] as int;
    name = el["name"] as String;
    blockedSites = el["blocked"] as bool;
  }

  Map<String, dynamic> toJson() => toMap();

  String toString() => toMap().toString();
}


class SHostDef {
  String ip;
  String name;

  SHostDef(this.ip, this.name);
  Map toMap() =>
      <String, dynamic>{"ip": ip, "name": name };
  String toString() => toMap().toString();
}

class HostReader {
  static const path = "../conf";
  static const defFile = 'all_hosts.conf';
  static const oDefFile = 'hosts.conf';
  static const ransomListUrl = "https://ransomwaretracker.abuse.ch/"
      "downloads/RW_DOMBL.txt";

  String _fName;

  //Address groups
  List<SHostDef> forbidden;
  List<SHostDef> ransomware;
  List<SHostDef> internet;
  List<SHostDef> local;
//  Map<int, String> comments = new Map();

  bool _mikroUpd = false; //Is updating mikrotiks

  List<SHostDef> get hosts {
    List<SHostDef> ret = new List()
         ..addAll(ransomware)
         ..addAll(forbidden)
         ..addAll(internet)
         ..addAll(local);

    return ret;
  }

  HostReader([String name]) {
    name = name ?? defFile;
    _fName = path+"/"+name;
  }

  String getName(String ip) {
    for (SHostDef sh in local)
      if (sh.ip == ip) return sh.name;
    return ip;
  }

  String getForbidden() {
    String res = "";
    for (int i = 0; i < forbidden.length; i++)
      res += forbidden[i].name+"\n";
    return res;
  }

  String getRansomware() {
    String res = "";
    for (int i = 0; i < ransomware.length; i++)
      res += ransomware[i].name+"\n";
    return res;
  }

  String getInternet() {
    String res = "";
    for (int i = 0; i < internet.length; i++)
       res += internet[i].ip+"\t"+internet[i].name+"\n";
    return res;
  }

  Future<Null> updateRansomware() async {
    String tmp = await http.getData(ransomListUrl);
    List<String> rows = tmp.split("\n");
    if (rows.length == 0) return; //Nothing to save
    ransomware.clear(); //Clear old ransomware
    for (int i = 0; i < rows.length; i++) {
      rows[i] = rows[i].trim().toLowerCase();
      int pos = rows[i].indexOf("#");
      if (pos > -1) rows[i] = rows[i].substring(0, pos); //Remove comments
      if (rows[i].isNotEmpty) ransomware.add(new SHostDef("127.0.0.2", rows[i]));
    }

    //Write addresses back
    await writeFile();
  }

  Future<bool> updateMikrotik(List<Mikrotik> mikrotiks) async {
    const mikroFile = 'mikrotik_defs.txt';
    const mikroBlockFile = 'mikrotik_defs_block.txt';

    if (_mikroUpd) return false; //Exit and return false if processing
    _mikroUpd = true;

    bool res = true;
    const mikroPrefix = "ip dns static";
    const mikroAdd = "add name";

     //Generate mikrotik definition structure

    //Remove old ransomware entries
    String mikroTxt = mikroPrefix+" remove [find comment=\"ransomware\"]\n";
    //Ransomware urls
    ransomware.forEach((SHostDef sH) {
      mikroTxt += mikroPrefix+" "+mikroAdd+" "+sH.name+" address="+sH.ip+" comment=\"ransomware\"\n";
    });

    String delForbidden = mikroPrefix+" remove [find comment=\"forbidden\"]\n";

    //Forbidden urls
    String mikroForbidden = delForbidden;
    forbidden.forEach((SHostDef sH) {
        //Is regex?
        if (sH.name.indexOf('*') > -1)
           mikroForbidden += mikroPrefix+" add regexp=\""+sH.name+"\" address="+sH.ip+" comment=\"forbidden\"\n";
          else
           mikroForbidden += mikroPrefix+" "+mikroAdd+" "+sH.name+" address="+sH.ip+" comment=\"forbidden\"\n";
     });

    mikroTxt+="/quit";

     File f1,f2;
     //Save Structure to file
     try {
       f1 = new File(mikroFile);
       await (f1.writeAsString(delForbidden+mikroTxt));
       f2 = new File(mikroBlockFile);
       await (f2.writeAsString(mikroForbidden+mikroTxt));
     } catch (err) {
       LogError(err.toString());
       res = false;
     }

    List<Future<String>> startPrg = new List();
    //Apply mikrotik list
    for (int i = 0; i < mikrotiks.length; i++ ) {
      LogData("Updating Mikrotik router ["+mikrotiks[i].name+'->'+mikrotiks[i].ip+':'+mikrotiks[i].port.toString()+"]...");

      //Decide which file
      String fName = mikroFile;
      if (mikrotiks[i].blockedSites) fName = mikroBlockFile;
      startPrg.add(ShTools.applyMikro(mikrotiks[i].ip, mikrotiks[i].port, fName));
    }

    //Await futures
    List<String> resPrg = await Future.wait(startPrg);
    for (int i = 0; i < resPrg.length;i++)
     if (resPrg[i].indexOf("interrupted") == -1) res = false; //Not finishing with interrupted

    //Remove file
    try {
      await f1.delete();
      await f2.delete();
    } catch(err) {
       LogError(err.toString());
       res = false;
    }


    _mikroUpd = false; //Reset status;
    return res;
  }

  void saveForbidden(String data) {
    data = data.replaceAll(" ", "\n").replaceAll(";", "\n").
        replaceAll(",", "\n").replaceAll("\t", "\n");
    List<String> addr = data.split("\n");
    forbidden.clear();
    for (String cAddr in addr) {
      String cA = cAddr.trim().toLowerCase();
      if (cA.isNotEmpty) forbidden.add(new SHostDef("127.0.0.1", cA));
    }
  }

  void saveInternet(String data) {
    List<String> addr = data.split("\n");
    internet.clear();
    for (String cAddr in addr) {
      //Replace tabs
      String cA = cAddr.trim().toLowerCase().replaceAll("\t", " ");
      if (cA.indexOf(" ") > -1) {
        int idx = cA.indexOf(" ");
        String ip = cA.substring(0, idx).trim();
        cA = cA.substring(idx+1, cA.length);
        if (cA.isNotEmpty) internet.add(new SHostDef(ip, cA));
      }
    }
  }

  Future<bool> readFile() async {
    String res;
    try {
      res = await new File(_fName).readAsString();
    } catch (err) {
      LogError (err.toString());
    }

    if (res == null || res.isEmpty) return false;

    //Init lists
    local = new List();
    internet = new List();
    forbidden = new List();
    ransomware = new List();

    res = res.trim();
    List<String> splRes = res.split("\n");
    for (int i = 0; i < splRes.length; i++) {
      String cRes = splRes[i];
      cRes = cRes.trim(); //Remove leading/trailing whitespaces
      if (cRes.isNotEmpty) {
        if (cRes.indexOf("#") != 0) {
        cRes = cRes.replaceAll("\t", " "); //Replace tabs
        List<String> splCRes = cRes.split(" ");
        String ip = splCRes[0]; //First part

        //Find name
        int j = 1;
        while(splCRes[j].isEmpty && j < splCRes.length) j++;
        String name = splCRes[j];

        //Create host
        SHostDef host = new SHostDef(ip,name);

        //Route host to its' place
        String ipLead = ip.substring(0, ip.indexOf('.'));
        switch (ipLead) {
          case "127":
             if (ip == "127.0.0.1") forbidden.add(host); //127.0.0.1 Will be used for blocked addresses
             if (ip == "127.0.0.2") ransomware.add(host); //127.0.0.2 Will be used for ransomware url addresses
            break;
          case "192": local.add(host);
            break;
          case "10": local.add(host);
            break;
          default:
            internet.add(host);
        }
       }
     }
    }

    return true;
  }


  String _genFile(List<SHostDef> retHosts) {
    String res = "";

    for (int i = 0; i < retHosts.length; i++) {
//      if (comments[idx+i] != null) res += comments[i]+"\n"; //Add comment for current row if any
      res += retHosts[i].ip + "\t\t" + retHosts[i].name + "\n";
    }

    return res;
  }

  String genFile({bool blocked = true, bool rans = true}) {
    String res = "#Host file generated at "+new DateTime.now().toString()+"\n";

    if (blocked) {
      res += "#Forbidden addresses\n";
      res += _genFile(forbidden);
    }

    if (rans) {
      res += "\n#Ransomware addresses\n";
      res += _genFile(ransomware);
    }

    res += "\n#Local addresses\n";
    res += _genFile(local);

    res += "\n#Internet addresses\n";
    res += _genFile(internet);

    return res;
  }

  void addLocalHost(String ip, String name) {
    int i = 0;
    while (i < local.length && local[i].ip != ip) i++; //Find position if exists
    if (i < local.length && local[i].ip == ip) local[i].name = name; //Found! change name only
     else local.add(new SHostDef(ip, name)); //Not found then add the host
  }

  void delLocal(String ip) {
    int i = 0;
    while (i < local.length && local[i].ip != ip) i++; //Find index
    if (i < local.length && local[i].ip == ip) local.removeAt(i); //Remove current index
  }

  Future<bool> writeFile({bool blocked = false}) async {
    File res;
    try {
      res = await new File(_fName).writeAsString(genFile());
      res = await new File(path+'/'+oDefFile).writeAsString(genFile(blocked:blocked));
    } catch (err) {
      LogError(err.toString());
    }

    return (res != null);
  }



}