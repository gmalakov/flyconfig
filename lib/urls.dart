library urls;

import 'package:route/url_pattern.dart';

final homeUrl = new UrlPattern(r'/ipconf/');
final loginUrl = new UrlPattern(r'/ipconf/login');
final logoutUrl = new UrlPattern(r'/ipconf/logout');
final reloadUrl = new UrlPattern(r'/ipconf/reload');
final applyUrl = new UrlPattern(r'/ipconf/applySettings');
final redirUrl = new UrlPattern(r'/ipconf/ports');
final hostUrl = new UrlPattern(r'/ipconf/hosts');

final delRedirUrl = new UrlPattern(r'/ipconf/delRedir/(.*)/(.*)'); //Delete port redirection
final editRedirUrl = new UrlPattern(r'/ipconf/editRedir/(.*)/(.*)/(.*)/(.*)/(.*)'); //Edit port redirection

final editRTargetUrl = new UrlPattern(r'/ipconf/editRTarget/(.*)/(.*)/(.*)');
final delRTargetUrl = new UrlPattern(r'/ipconf/delRTarget/(.*)/(.*)/(.*)');

final editHostUrl = new UrlPattern(r'/ipconf/editHost/(.*)/(.*)/(.*)');
final delHostUrl = new UrlPattern(r'/ipconf/delHost/(.*)/(.*)/(.*)');

final newPasswordUrl = new UrlPattern(r'/ipconf/newPass/(.*)/(.*)'); //New password
final editVarsUrl = new UrlPattern(r'/ipconf/editVars'); //Global variables
final forbiddenUrl = new UrlPattern(r'/ipconf/forbidden');
final saveForbiddenUrl = new UrlPattern(r'/ipconf/saveForbidden');
final inetHostsUrl = new UrlPattern(r'/ipconf/inetHosts');
final saveInetUrl = new UrlPattern(r'/ipconf/saveInet');
final globalsUrl = new UrlPattern(r'/ipconf/globals');
final saveGlobalsUrl = new UrlPattern(r'/ipconf/saveGlobals');

final watchUrl = new UrlPattern(r'/ipconf/watch');
final getWatchIntervalUrl = new UrlPattern(r'/ipconf/getWatchInterval');
final saveWatchUrl = new UrlPattern(r'/ipconf/saveWatch');

final arpget = new UrlPattern(r'/ipconf/arpget'); //Get mac by arp

final ticketsUrl = new UrlPattern(r'/ipconf/tickets'); //Ticket display
final newTicketUrl = new UrlPattern(r'/ipconf/newTicket/(.*)/(.*)'); //New ticket /ip/time(minutes)
final delTicketUrl = new UrlPattern(r'/ipconf/delTicket/(.*)'); // Del ticket /ip

final authUrl = [homeUrl,
logoutUrl, reloadUrl, applyUrl, redirUrl, hostUrl,
delRedirUrl, editRedirUrl,
delRTargetUrl, editRTargetUrl,
forbiddenUrl, saveForbiddenUrl,
logoutUrl, reloadUrl, applyUrl, newPasswordUrl,
editRedirUrl, delRedirUrl,
editHostUrl, delHostUrl,
inetHostsUrl, saveInetUrl,
globalsUrl, saveGlobalsUrl,
watchUrl, getWatchIntervalUrl, saveWatchUrl,
arpget, ticketsUrl, newTicketUrl, delTicketUrl];

final jsUrl = new UrlPattern(r'/ipconf/js/(.*)');
final cssUrl = new UrlPattern(r'/ipconf/css/(.*)');
final imgUrl = new UrlPattern(r'/ipconf/img/(.*)');
final img2Url = new UrlPattern(r'/ipconf/images/(.*)');

final statUrls = [jsUrl, cssUrl, imgUrl, img2Url];
