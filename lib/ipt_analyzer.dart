library ipt_analyzer;

import 'package:tools/tools.dart';
import 'dart:async';
import 'shTools.dart';

class ChainList {
  List<List<String>> _inputChain, _forwardChain, _outputChain;
  ChainList(List<String> inputChain, List<String> forwardChain, List<String> outputChain) {
    _inputChain = splitChain(inputChain);
    _forwardChain = splitChain(forwardChain);
    _outputChain = splitChain(outputChain);
  }

  List<List<String>> splitChain(List<String> chain) {
    if (chain == null) chain = [];

    List<List<String>> res = [];
    for (int i = 0; i < chain.length; i++) {
      List<String> cRow = chain[i].trim().split(" ");
      res.add(cRow);
    }

   return res;
  }

  int _findPos(List<List<String>> chain, String ip) {
    for (int i = 0; i < chain.length; i++)
      if (chain[i].length > 4 && chain[i][3] == ip) return i;

    return -1;
  }

  int inputPos(String ip) => _findPos(_inputChain, ip);
  int forwardPos(String ip) => _findPos(_forwardChain, ip);
  int outputPos(String ip) => _findPos(_outputChain, ip);

  int inputPosDel(String ip) {
    int res = inputPos(ip);
    if (res > -1) _inputChain.removeAt(res);
    return res;
  }

  int forwardPosDel(String ip) {
    int res = forwardPos(ip);
    if (res > -1) _forwardChain.removeAt(res);
    return res;
  }

  int outputPosDel(String ip) {
    int res = outputPos(ip);
    if (res > -1) _outputChain.removeAt(res);
    return res;
  }

  //Will be used when adding new rules to an existing chain with pending deletes
  void insertInput(String ip) {
    _inputChain.insert(0, ['0', '0', '0', ip, '0']); //Add fake line to input chain with this ip
  }
  void insertForward(String ip) {
    _forwardChain.insert(0, ['0', '0', '0', ip, '0']); //Add fake line to input chain with this ip
  }
  void insertOutput(String ip) {
    _outputChain.insert(0, ['0', '0', '0', ip, '0']); //Add fake line to input chain with this ip
  }


  Map<String, dynamic> toMap() => {
    "inputChain": _inputChain,
    "forwardChain": _forwardChain,
    "outputChain": _outputChain
  };

  String toString() => toMap().toString();
}

class IptAnalyzer {
  static const String input = "Chain INPUT";
  static const String forward = "Chain FORWARD";
  static const String output = "Chain OUTPUT";
  static const String desc =
      "target prot opt source destination";

  static List<String> _ipCache;
  static Map<String, String> _dictionary = {};

  static List<String> _delHosts = [];
  static List<Completer<bool>> _delQueue = [];
  static ChainList _chainList;

  static Future<ChainList> getRules() async {
    int i = 0;

    List<String> getChain(List<String> rows, String chain) {
      if (i >= rows.length) return null;
      List<String> retRows = []; //Returning rows

      while (i < rows.length && rows[i].indexOf(chain) == -1) i++; //Find input
      i++; //next row
      if (i < rows.length) {
        String cRow = rows[i].trim();
        if (cRow.indexOf(desc) > -1) {
          //Description row ?
          i++; //Next row (should be first data row)
          //Trim first row if we are not at the end of the list
          if (i < rows.length) rows[i] = rows[i].trim();
          while (i < rows.length && rows[i].isNotEmpty) {
            retRows.add(rows[i]);
            i++;
            //Trim next row if not end of the list
            if (i < rows.length) rows[i] = rows[i].trim();
          }
        }

        i++; //Skip empty row
      }
      return retRows;
    }

    String res = await ShTools.run("iptables -n -L", "flyfs", trim: false);

    res = res.replaceAll("\t", " "); //Remove tabs
    while (res.indexOf("  ") > -1) res = res.replaceAll("  ", " "); //Remove double spaces and so
    List<String> rows = res.split('\n');

    //Find input
    List<String> inputRows = getChain(rows, input);
    List<String> forwardRows = getChain(rows, forward);
    List<String> outputRows = getChain(rows, output);

    return new ChainList(inputRows, forwardRows, outputRows);
  }

  //Find host position in rules and delete it
  static Future<bool> _delHost(ChainList ch, String ip) async {
    bool found = false;
    int pos = ch.inputPosDel(ip);
    if (pos > -1) {
      await ShTools.run("iptables -D INPUT ${pos+1}", "flyfs");
      found = true;
    }

    pos = ch.forwardPosDel(ip);
    if (pos > -1) {
      await ShTools.run("iptables -D FORWARD ${pos+1}", "flyfs");
      found = true;
    }

    return found;
  }

  static Future _runDelQueue() async {
    if (_delHosts.length == 0) return;
    //Get new chainlist if null
    _chainList ??= await IptAnalyzer.getRules();

    String ip = _delHosts[0];
    _delHosts.removeAt(0);
    Completer<bool> comp = _delQueue[0];
    _delQueue.removeAt(0);

    comp.complete(await _delHost(_chainList, ip));

    if (_delHosts.length > 0) _runDelQueue();
     else _chainList = null;
    //Remove chainlist on empty queue
  }

  static Future<bool> delHost(String ip) async {

    _delHosts.add(ip);
    _delQueue.add(new Completer<bool>());
    int idx = _delQueue.length-1;
    //First added host then _runQueue
    if (_delHosts.length == 1) _runDelQueue();

    return _delQueue[idx].future;
  }


  static Future<bool> addHost(String ip) async {
    bool res = await delHost(ip); //Delete previous entries if any

    String mac = _dictionary[ip];
    String macAddon = "";
    if (mac != null)
      macAddon = "-m mac --mac-source $mac";

    await ShTools.run("iptables -I INPUT -s $ip $macAddon -j ACCEPT", "flyfs"); //Insert on top of input chain
    await ShTools.run("iptables -I FORWARD -s $ip $macAddon -j ACCEPT", "flyfs"); //Insert on top of forward chain

    //Add fake line to chainlist to maintain old chain positions if chainlist is loaded and deletes are still pending
    if (_chainList != null)
      _chainList
        ..insertInput(ip)
        ..insertForward(ip);

    return res; //Return found or not found before
  }

  static Future<List<String>> getWifis(String subnet) async {
    String res = await ShTools.run("arp -an | grep $subnet", "flyfs");
    List<String> rows = res.split("?");
    List<String> ips = [];

    RegExp ipR = new RegExp(r"\((.*)\)");
    RegExp macR = new RegExp(r"([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})");

    for (int i = 0; i < rows.length; i++) {
      Match m = ipR.firstMatch(rows[i]);
      if (m != null && m.groupCount > 0) {
        //ips.add(m.group(1));
        Match macM = macR.firstMatch(rows[i]);
        if (macM != null) {
          ips.add(m.group(1)); // -> Only add ip address if there is MAC present
          String mac = macM.group(0);
          _dictionary[ips.last] = mac;
        }
      }
    }
    ips.sort(); //Sort ip addresses

    if (ips.isNotEmpty) {
      _ipCache = ips;
      return ips;
    }

    return _ipCache;
    }

}

class Tickets {
  static const checkMin = 1;

  List<String> _ips = [];
  List<DateTime> _endTimes = [];

  StreamController<String> _onDelTicket = new StreamController();
  Stream<String> get onDelTicket => _onDelTicket.stream;

  StreamController<String> _onAddTicket = new StreamController();
  Stream<String> get onAddTicket => _onAddTicket.stream;

  static Tickets _tickets = new Tickets();
  static Tickets get instance => _tickets;

  Tickets() {
    new Timer.periodic(const Duration(minutes: checkMin), (_) => checkTimes());
  }

  void addTicket(String ip, int minutes) {
    _ips.add(ip);
    _endTimes.add(new DateTime.now().add(new Duration(minutes: minutes)));
    _onAddTicket.add(ip);
  }

  DateTime getEndTime(String ip) {
    int i = _ips.indexOf(ip);
    if (i > -1) return _endTimes[i];
     else return null;
  }


  void delTicket(String ip) {
    int i = _ips.indexOf(ip);

    _onDelTicket.add(_ips[i]); //Broadcast deleting ip
    _ips.removeAt(i);
    _endTimes.removeAt(i);
  }

  void checkTimes() {
    DateTime now = new DateTime.now();
    for (int i = 0; i < _ips.length; i++)
      if (_endTimes[i].isBefore(now)) delTicket(_ips[i]);
  }

  List<Map<String, String>> getTickets() {
    checkTimes();
    List<Map<String, String>> res = [];
    for (int i = 0; i < _ips.length; i++)
      res.add({"ip": _ips[i], "endTime": Dates.bgDateFull(_endTimes[i]) });
    res.sort((a,b) => a["endTime"].compareTo(b["endTime"]));

    return res;
  }


}