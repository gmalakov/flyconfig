library probe;

import "dart:io";
import "dart:async";
import "package:tools/logger.dart";

class Probe {
  static const dPort = 53;
  static const probes = 2;
  static const Duration waits = const Duration(seconds: 2);

  //Dns probe
  Map<int, InternetAddress> _targets = new Map();

  void setTarget(int id, String target) =>
      _targets[id] = new InternetAddress(target);


  Future<bool> simpleProbe(int id) async {
    Socket sock;
    try {
    sock = await(Socket.connect(_targets[id], dPort)
        ..catchError((dynamic error) => LogError(error.toString()))
        ..timeout(const Duration(seconds: 5),
            onTimeout: () { LogError("Timeout connecting "+_targets[id].address); return null; }));
      sock?.done?.catchError((dynamic err) => LogError(err.toString()));
    } catch (err) {
      LogError(err.toString());
    }

    if (sock == null) return false;
     else { //Socket is connected -> return true
       await sock.close();
       return true;
    }
  }

  Future<bool> here(int id) async {
    bool isHere = false; int i = 0;
    while (i < probes && !isHere) {
      isHere = await simpleProbe(id);
      i++;
      if (!isHere) {
        LogData("Still not finding host "+_targets[id].address);
        await new Future<Null>.delayed(waits); //Wait delaytime if host is not found still
      }
    }

    return isHere; //Return found status
  }

  Future<Map<int,bool>> probeAll() async {
    Map<int,bool> res = new Map();
    for (int key in _targets.keys)
      res[key] = await here(key);

    return res;
  }

}